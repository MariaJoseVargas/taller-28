from django.apps import AppConfig


class RegistrarNotasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'registrar_notas'
